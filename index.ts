// import EscposGenerator from 'escpos-generator';
import express = require('express');

import { WebClientPrint } from './web-client-print';

const server = express();
server.get('/command', (req: express.Request, res: express.Response) => {
  // const escPosEncoder = new EscPosEncoder();
  // const escPosCommands = escPosEncoder
  //   .initialize()
  //   .text('Hello, world!')
  //   .encode();

  // const escPosCommandsHex = Buffer.from(escPosCommands).toString('hex');

  // const generator = new EscposGenerator();
  // const escPosCommands = generator
  //   .init()
  //   .selectCharacterCodeTable(19, 858)
  //   .font(EscposGenerator.FONT_NORMAL)
  //   .align(EscposGenerator.CENTER)
  //   .bold()
  //   .text('Mario bros')
  //   .bold(false)
  //   .newLine()
  //   .text("It's-a me, Mario!")
  //   .newLine()
  //   .cutPaper(EscposGenerator.CUT_PAPER_FULL, 0x80)
  //   .toArray(true);

  const webClientPrint = new WebClientPrint();
  webClientPrint.sendPrinterCommands(
    res,
    `
    @
    Halo semuanya!
    `,
  );
});

server.listen(5000);
